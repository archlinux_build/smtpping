# smtpping

A simple, portable tool for measuring SMTP server delay, delay variation and throughput

Read more here: [smtpping](https://github.com/halon/smtpping)

Download **smtpping** package for Archlinux from here:
[smtpping](https://gitlab.com/archlinux_build/smtpping/-/jobs/artifacts/master/browse?job=run-build)